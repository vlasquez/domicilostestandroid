package com.example.andresvelasquez.domiciliostest.network.request.stop;

import com.example.andresvelasquez.domiciliostest.network.callbacks.StopsCallback;
import com.example.andresvelasquez.domiciliostest.network.response.StopsResponse;

/**
 * Created by AndresVelasquez on 5/03/18.
 */

public class StopRequest implements StopsCallback {
    public interface OnStopsListener{
        void didGetStops(StopsResponse stopsResponse);
        void didGetError(String error);
    }
    private final StopsRequestImp stopsRequestImp;
    private final OnStopsListener stopsListener;
    private static StopRequest instance;

    /**
     * Constructor singleton para la clase StopsRequest
     * @param context
     */
    private StopRequest(Object context) {
        stopsRequestImp = new StopsRequestImp(this);
        if(context instanceof OnStopsListener){
            stopsListener = (OnStopsListener) context;
        }
        else{
            throw new RuntimeException(context.toString() + "must implement OnStopsListener");
        }
    }

    public static StopRequest getInstance(Object context){
        if(instance == null){
            instance = new StopRequest(context);
        }
        return instance;
    }

    /**
     * Método que ejecuta la petición
     */
    public void getStops(String url){
        stopsRequestImp.requestStops(url);
    }

    @Override
    public void onError(Throwable error) {
        stopsListener.didGetError(error.toString());
    }

    @Override
    public void onStopsResponse(StopsResponse stopsResponse) {
        try{
            stopsListener.didGetStops(stopsResponse);
        }
        catch (Exception ex){
            stopsListener.didGetError(ex.toString());
        }
    }
}
