package com.example.andresvelasquez.domiciliostest.network.callbacks;

/**
 * Created by AndresVelasquez on 5/03/18.
 */

public interface Callback {
    void onError(Throwable error);
}
