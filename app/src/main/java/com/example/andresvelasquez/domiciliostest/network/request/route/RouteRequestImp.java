package com.example.andresvelasquez.domiciliostest.network.request.route;

import com.example.andresvelasquez.domiciliostest.network.APIClient;
import com.example.andresvelasquez.domiciliostest.network.APIService;
import com.example.andresvelasquez.domiciliostest.network.callbacks.RouteCallback;
import com.example.andresvelasquez.domiciliostest.network.response.RouteResponse;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;

/**
 * Created by AndresVelasquez on 5/03/18.
 */

public class RouteRequestImp implements IRouteRequest {
    private final APIService mService;
    private final RouteCallback callback;

    public RouteRequestImp(Object context) {
        Retrofit retrofit = APIClient.getInstance();
        mService = retrofit.create(APIService.class);
        if(context instanceof RouteCallback)
            callback = (RouteCallback) context;
        else
            throw new RuntimeException(context.toString() + " must implement RouteCallback");
    }

    @Override
    public void requestRoutes() {
        mService.getRoutes()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread())
                .subscribe(new Observer<RouteResponse>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(RouteResponse routeResponse) {
                        callback.onRouteResponse(routeResponse);
                    }

                    @Override
                    public void onError(Throwable e) {
                        callback.onError(e);
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }
}
