package com.example.andresvelasquez.domiciliostest.network;

import com.example.andresvelasquez.domiciliostest.network.response.RouteResponse;
import com.example.andresvelasquez.domiciliostest.network.response.StopsResponse;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Url;


/**
 * Created by AndresVelasquez on 5/03/18.
 */

public interface APIService {

    /**
     * Consumo de servicio para obtener informacion de las rutas
     * @return RouteResponse
     */
    @GET("bins/10yg1t")
    Observable<RouteResponse> getRoutes();

    /**
     * Obtiene la localizacion de las paradas del vehiculo
     * @param url
     * @return
     */
    @GET()
    Observable<StopsResponse> getStops(@Url String url);
}
