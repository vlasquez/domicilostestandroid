package com.example.andresvelasquez.domiciliostest.activities.route.view;

import com.example.andresvelasquez.domiciliostest.network.response.RouteResponse;

/**
 * Created by AndresVelasquez on 5/03/18.
 */

public interface IMainView {
    void bindRoutes(RouteResponse routeResponse);
    void showProgress();
    void showToast(String error);
}
