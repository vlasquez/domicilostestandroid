package com.example.andresvelasquez.domiciliostest.activities.stops.model;

import com.example.andresvelasquez.domiciliostest.network.request.stop.StopRequest;

/**
 * Created by AndresVelasquez on 5/03/18.
 */

public class StopsInteractor implements IStopsInteractor {
    @Override
    public void getStops(StopRequest.OnStopsListener listener, String url) {
        StopRequest.getInstance(listener).getStops(url);
    }
}
