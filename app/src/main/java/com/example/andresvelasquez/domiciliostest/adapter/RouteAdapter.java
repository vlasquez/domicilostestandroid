package com.example.andresvelasquez.domiciliostest.adapter;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.andresvelasquez.domiciliostest.models.Route;
import com.example.andresvelasquez.domiciliostest.R;
import com.example.andresvelasquez.domiciliostest.util.CircleTransform;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by andresvelasquez on 4/03/18.
 */

public class RouteAdapter extends RecyclerView.Adapter<RouteAdapter.ViewHolder>{
    private Context mContext;
    private int layout;
    private OnItemClickListener listener;
    private List<Route> routes;

    public RouteAdapter(int layout, OnItemClickListener listener, List<Route> routes) {
        this.layout = layout;
        this.listener = listener;
        this.routes = routes;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(layout,parent,false);
        mContext = parent.getContext();
        ViewHolder vh = new ViewHolder(v);
        return vh   ;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bind(routes.get(position),listener);
    }

    @Override
    public int getItemCount() {
        return this.routes.size();
    }

    /**
     * ViewHolder
     */
    public class ViewHolder extends RecyclerView.ViewHolder{
        public ImageView routeImageView;
        public TextView txtRouteName;
        public TextView txtRouteId;
        public TextView txtRouteDescription;

        public ViewHolder(View itemView) {
            super(itemView);
            this.routeImageView = (ImageView) itemView.findViewById(R.id.routeImageView);
            this.txtRouteName = (TextView) itemView.findViewById(R.id.txtRouteName);
            this.txtRouteId = (TextView) itemView.findViewById(R.id.txtRouteId);
            this.txtRouteDescription = (TextView) itemView.findViewById(R.id.txtRouteDescription);
        }
        public void bind (final Route route, final OnItemClickListener listener){
            this.txtRouteDescription.setText(route.getDescription());
            this.txtRouteId.setText(route.getId().toString());
            this.txtRouteName.setText(route.getName());
            Picasso.with(mContext).load(route.getImg_url()).fit().transform(new CircleTransform()).into(routeImageView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.OnItemClick(route,getAdapterPosition());
                }
            });
        }
    }

    /**
     * OnItemClickListener
     */
    public interface OnItemClickListener{
        void OnItemClick(Route route, int position);
    }
}
