package com.example.andresvelasquez.domiciliostest.activities.stops.view;

import com.example.andresvelasquez.domiciliostest.network.response.StopsResponse;

/**
 * Created by AndresVelasquez on 5/03/18.
 */

public interface IStopsView {
    void bindStops(StopsResponse stopsResponse);
    void showProgress();
    void showToast(String error);
}
