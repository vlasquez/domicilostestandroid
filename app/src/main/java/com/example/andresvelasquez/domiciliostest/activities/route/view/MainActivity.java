package com.example.andresvelasquez.domiciliostest.activities.route.view;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import com.example.andresvelasquez.domiciliostest.R;
import com.example.andresvelasquez.domiciliostest.activities.route.presenter.IRoutePresenter;
import com.example.andresvelasquez.domiciliostest.activities.route.presenter.RoutePresenter;
import com.example.andresvelasquez.domiciliostest.activities.stops.view.MapsActivity;

import com.example.andresvelasquez.domiciliostest.activities.stops.view.StopActivity;
import com.example.andresvelasquez.domiciliostest.adapter.RouteAdapter;
import com.example.andresvelasquez.domiciliostest.models.Route;
import com.example.andresvelasquez.domiciliostest.network.response.RouteResponse;

import java.util.ArrayList;
import java.util.List;

import static com.example.andresvelasquez.domiciliostest.util.AppConstants.INTERNET_PERMISSION_CODE;

public class MainActivity extends AppCompatActivity implements IMainView {
    RecyclerView.Adapter mAdapter;
    RecyclerView.LayoutManager mLayoutManager;
    RecyclerView mRecyclerView;
    IRoutePresenter routePresenter;
    private List<Route> routes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if(ContextCompat.checkSelfPermission(this,
                Manifest.permission.INTERNET)
                != PackageManager.PERMISSION_GRANTED){
            askPermissions();
        }
        routePresenter = new RoutePresenter(this);
        mRecyclerView = (RecyclerView) findViewById(R.id.routeRecyclerView);
        mLayoutManager = new LinearLayoutManager(this);

        this.routes = new ArrayList<Route>();


        mAdapter = new RouteAdapter(R.layout.route_item, new RouteAdapter.OnItemClickListener() {
            @Override
            public void OnItemClick(Route route, int position) {
                Intent i = new Intent(MainActivity.this, StopActivity.class);
                i.putExtra("route",route);
                startActivity(i);
            }
        },this.routes);

        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        showProgress();
        Log.d("act","onResume");

    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d("act","onStop");
        this.routes.clear();
        mAdapter.notifyDataSetChanged();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("act","onDestroy");

    }

    @Override
    public void showProgress() {
        //TODO: Mostrar AlertDialog de progreso
        routePresenter.onGetRoutes();

    }

    @Override
    public void bindRoutes(RouteResponse routeResponse) {
        this.routes.addAll(routeResponse.school_buses);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void showToast(String error) {
        Toast.makeText(this,error,Toast.LENGTH_SHORT).show();
        Log.e("error",error);
    }

    private void askPermissions(){
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.INTERNET)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.INTERNET)) {

            } else {

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.READ_CONTACTS},
                        INTERNET_PERMISSION_CODE);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case INTERNET_PERMISSION_CODE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                } else {
                    showToast("La aplicacion necesita permisos para funcionar");
                    finish();
                }
                return;
            }
        }
    }
}
