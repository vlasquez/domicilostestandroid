package com.example.andresvelasquez.domiciliostest.network.callbacks;

import com.example.andresvelasquez.domiciliostest.network.response.RouteResponse;

/**
 * Created by AndresVelasquez on 5/03/18.
 */

public interface RouteCallback extends Callback {
 void onRouteResponse(RouteResponse routeResponse);
}
