package com.example.andresvelasquez.domiciliostest.models;

import java.io.Serializable;

/**
 * Created by AndresVelasquez on 5/03/18.
 */

public class Stop implements Serializable {
    Double lat;
    Double lng;

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }


}
