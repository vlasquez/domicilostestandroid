package com.example.andresvelasquez.domiciliostest.network.request.stop;

import com.example.andresvelasquez.domiciliostest.network.APIClient;
import com.example.andresvelasquez.domiciliostest.network.APIService;
import com.example.andresvelasquez.domiciliostest.network.callbacks.RouteCallback;
import com.example.andresvelasquez.domiciliostest.network.callbacks.StopsCallback;
import com.example.andresvelasquez.domiciliostest.network.response.StopsResponse;

import io.reactivex.Observer;
import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;

/**
 * Created by AndresVelasquez on 5/03/18.
 */

public class StopsRequestImp implements IStopsRequest {
    private final APIService mService;
    private final StopsCallback callback;

    public StopsRequestImp(Object context) {
        Retrofit retrofit = APIClient.getInstance();
        mService = retrofit.create(APIService.class);
        if(context instanceof StopsCallback)
            callback = (StopsCallback) context;
        else
            throw new RuntimeException(context.toString() + " must implement StopsCallback");

    }

    @Override
    public void requestStops(String url) {
        mService.getStops(url)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread())
                .subscribe(new Observer<StopsResponse>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(StopsResponse stopsResponse) {
                        callback.onStopsResponse(stopsResponse);
                    }

                    @Override
                    public void onError(Throwable e) {
                        callback.onError(e);
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }
}
