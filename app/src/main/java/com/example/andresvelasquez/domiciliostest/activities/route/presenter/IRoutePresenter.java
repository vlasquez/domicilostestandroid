package com.example.andresvelasquez.domiciliostest.activities.route.presenter;

/**
 * Created by AndresVelasquez on 5/03/18.
 */

public interface IRoutePresenter {
    void onPause();
    void onGetRoutes();
}
