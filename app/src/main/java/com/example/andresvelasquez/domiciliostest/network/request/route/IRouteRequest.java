package com.example.andresvelasquez.domiciliostest.network.request.route;

/**
 * Created by AndresVelasquez on 5/03/18.
 */

public interface IRouteRequest {
    void requestRoutes();
}
