package com.example.andresvelasquez.domiciliostest.activities.route.presenter;

import com.example.andresvelasquez.domiciliostest.activities.route.model.IRouteInteractor;
import com.example.andresvelasquez.domiciliostest.activities.route.model.RouteInteractor;
import com.example.andresvelasquez.domiciliostest.activities.route.view.IMainView;
import com.example.andresvelasquez.domiciliostest.models.Route;
import com.example.andresvelasquez.domiciliostest.network.request.route.RouteRequest;
import com.example.andresvelasquez.domiciliostest.network.response.RouteResponse;

/**
 * Created by AndresVelasquez on 5/03/18.
 */

public class RoutePresenter implements IRoutePresenter, RouteRequest.OnRouteListener {

    private IMainView mainView;
    private IRouteInteractor routeInteractor;

    public RoutePresenter(IMainView mainView) {
        this.mainView = mainView;
        this.routeInteractor = new RouteInteractor();
    }

    @Override
    public void onPause() {

    }

    /**
     * Se comunica con el interactor para proceder con la peticion del listado de rutas
     */
    @Override
    public void onGetRoutes() {
        routeInteractor.getRoutes(this);
    }


    /**
     * Retorna el listado de rutas
     * @param routeResponse
     */
    @Override
    public void didGetRoutes(RouteResponse routeResponse) {
        mainView.bindRoutes(routeResponse);
    }

    /**
     * Retorna error en caso de que haya fallado la peticion del servicio
      * @param error
     */
    @Override
    public void didGetError(String error) {
        mainView.showToast(error);
    }
}
