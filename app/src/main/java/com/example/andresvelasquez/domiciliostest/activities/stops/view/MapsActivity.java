package com.example.andresvelasquez.domiciliostest.activities.stops.view;

import android.content.Intent;
import android.os.CountDownTimer;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.example.andresvelasquez.domiciliostest.R;
import com.example.andresvelasquez.domiciliostest.activities.stops.presenter.IStopsPresenter;
import com.example.andresvelasquez.domiciliostest.activities.stops.presenter.StopsPresenter;
import com.example.andresvelasquez.domiciliostest.models.Route;
import com.example.andresvelasquez.domiciliostest.models.Stop;
import com.example.andresvelasquez.domiciliostest.network.response.StopsResponse;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.ArrayList;
import java.util.List;

public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback,IStopsView {

    private GoogleMap mMap;
    IStopsPresenter stopsPresenter;
    private StopsResponse stopsResponse;
    private Route route;
    private TextView txtEstimatedTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        stopsPresenter = new StopsPresenter(this);
        txtEstimatedTime = (TextView) findViewById(R.id.txtEstimatedTime);
        Log.i("ruta","entro aqui");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Intent i = getIntent();
        this.route = (Route) i.getSerializableExtra("route");
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        showProgress();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
    }

    @Override
    protected void onStop() {
        super.onStop();
        mMap.clear();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void bindStops(StopsResponse stopsResponse) {
        this.stopsResponse = stopsResponse;

        int s = 1;
        List<LatLng> points=new ArrayList<LatLng>();
        PolylineOptions polyline = new PolylineOptions()
                    .width(10)
                    .color(R.color.primary)
                    .geodesic(true);

        for (Stop stop : this.stopsResponse.stops){
            points.add(new LatLng(stop.getLat(),stop.getLng()));

            mMap.addMarker(new MarkerOptions()
                    .position(new LatLng(stop.getLat(),stop.getLng()))
                    .title("Parada nº "+ s + " " + route.getName()));
            polyline.add(new LatLng(stop.getLat(),stop.getLng()));
                    ++s;
        }
        mMap.addPolyline(polyline);
        CameraUpdate center = CameraUpdateFactory.newLatLng(points.get(0));
        CameraUpdate zoom = CameraUpdateFactory.zoomTo(15);
        mMap.moveCamera(center);
        mMap.animateCamera(zoom);
        setEstimatedTime(this.stopsResponse.estimated_time_milliseconds);
        Log.i("ruta",String.valueOf(this.stopsResponse.estimated_time_milliseconds));
    }

    @Override
    public void showProgress() {
        //TODO: Mostrar AlertDialog de progreso
        try{
            stopsPresenter.onGetStops(this.route.getStops_url());
            Log.i("ruta",String.valueOf(this.route.getStops_url()));

        }
        catch (Exception ex)
        {
            showToast(ex.toString());
        }

    }
    private void setEstimatedTime(int time){
        CountDownTimer ct = new CountDownTimer(time,1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                String v = String.format("%02d", millisUntilFinished/60000);
                int va = (int)( (millisUntilFinished%60000)/1000);
                txtEstimatedTime.setText(v+":"+String.format("%02d",va));
            }

            @Override
            public void onFinish() {

            }
        };
        ct.start();

    }
    @Override
    public void showToast(String error) {
        Toast.makeText(this,error,Toast.LENGTH_SHORT).show();
    }
}
