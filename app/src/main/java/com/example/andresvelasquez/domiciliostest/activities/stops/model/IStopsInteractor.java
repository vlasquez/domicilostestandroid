package com.example.andresvelasquez.domiciliostest.activities.stops.model;

import com.example.andresvelasquez.domiciliostest.network.request.stop.StopRequest;

/**
 * Created by AndresVelasquez on 5/03/18.
 */

public interface IStopsInteractor {
    void getStops(StopRequest.OnStopsListener listener, String url);
}
