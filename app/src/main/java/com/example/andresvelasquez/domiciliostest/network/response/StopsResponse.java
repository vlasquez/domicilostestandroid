package com.example.andresvelasquez.domiciliostest.network.response;

import com.example.andresvelasquez.domiciliostest.models.Stop;

import java.io.Serializable;
import java.util.List;

/**
 * Created by AndresVelasquez on 5/03/18.
 */

public class StopsResponse implements Serializable{
    boolean response;
    public List<Stop> stops;
    public int estimated_time_milliseconds;
    public int retry_time_milliseconds;


}
