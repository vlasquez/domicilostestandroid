package com.example.andresvelasquez.domiciliostest.models;

import java.io.Serializable;

/**
 * Created by andresvelasquez on 4/03/18.
 */

public class Route implements Serializable{
    Integer id;
    String name;
    String description;
    String img_url;
    String stops_url;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImg_url() {
        return img_url;
    }

    public void setImg_url(String img_url) {
        this.img_url = img_url;
    }

    public String getStops_url() {
        return stops_url;
    }

    public void setStops_url(String stops_url) {
        this.stops_url = stops_url;
    }
}
