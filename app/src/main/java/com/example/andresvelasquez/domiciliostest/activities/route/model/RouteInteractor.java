package com.example.andresvelasquez.domiciliostest.activities.route.model;

import android.content.Context;

import com.example.andresvelasquez.domiciliostest.network.request.route.RouteRequest;
import com.example.andresvelasquez.domiciliostest.network.response.RouteResponse;

/**
 * Created by AndresVelasquez on 5/03/18.
 */

public class RouteInteractor implements IRouteInteractor {

    @Override
    public void getRoutes(RouteRequest.OnRouteListener listener) {
        RouteRequest.getInstance(listener).getRoutes();
    }

}
