package com.example.andresvelasquez.domiciliostest.network.callbacks;


import com.example.andresvelasquez.domiciliostest.network.response.StopsResponse;

/**
 * Created by AndresVelasquez on 5/03/18.
 */

public interface StopsCallback extends Callback {
    void onStopsResponse(StopsResponse stopsResponse);
}
