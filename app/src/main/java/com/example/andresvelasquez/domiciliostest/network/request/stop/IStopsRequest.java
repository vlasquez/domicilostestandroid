package com.example.andresvelasquez.domiciliostest.network.request.stop;

/**
 * Created by AndresVelasquez on 5/03/18.
 */

public interface IStopsRequest {
    void requestStops(String url);
}
