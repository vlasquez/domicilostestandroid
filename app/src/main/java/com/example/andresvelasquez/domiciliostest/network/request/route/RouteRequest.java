package com.example.andresvelasquez.domiciliostest.network.request.route;

import com.example.andresvelasquez.domiciliostest.network.callbacks.RouteCallback;
import com.example.andresvelasquez.domiciliostest.network.response.RouteResponse;

/**
 * Created by AndresVelasquez on 5/03/18.
 */

public final class RouteRequest implements RouteCallback {
    public interface OnRouteListener{
        void didGetRoutes(RouteResponse routeResponse);
        void didGetError(String error);
    }

    private final RouteRequestImp routeRequestImp;
    private final OnRouteListener routeListener;
    private static RouteRequest instance;

    /**
     * Constructor singleton para la clase RouteRequest
     * @param context
     */
    private RouteRequest(Object context) {
        routeRequestImp = new RouteRequestImp(this);
        if(context instanceof OnRouteListener)
            routeListener = (OnRouteListener) context;
        else
            throw new RuntimeException(context.toString() + "must implement OnRouteListener");
    }
    public static RouteRequest getInstance(Object context){
        if(instance == null){
            instance = new RouteRequest(context);
        }
        return instance;
    }
    /**
     * Método que ejecuta la petición
     */
    public void getRoutes(){
        routeRequestImp.requestRoutes();
    }

    @Override
    public void onError(Throwable error) {
        routeListener.didGetError(error.toString());
    }

    @Override
    public void onRouteResponse(RouteResponse routeResponse) {
        try{
            routeListener.didGetRoutes(routeResponse);
        }
        catch (Exception ex){
            routeListener.didGetError(ex.toString());
        }

    }
}
