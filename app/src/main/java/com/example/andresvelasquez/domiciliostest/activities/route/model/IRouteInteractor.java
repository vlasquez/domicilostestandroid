package com.example.andresvelasquez.domiciliostest.activities.route.model;

import com.example.andresvelasquez.domiciliostest.network.request.route.RouteRequest;


/**
 * Created by AndresVelasquez on 5/03/18.
 */

public interface IRouteInteractor {
    void getRoutes(RouteRequest.OnRouteListener listener);

}
