package com.example.andresvelasquez.domiciliostest.activities.stops.presenter;

import com.example.andresvelasquez.domiciliostest.activities.stops.model.IStopsInteractor;
import com.example.andresvelasquez.domiciliostest.activities.stops.model.StopsInteractor;
import com.example.andresvelasquez.domiciliostest.activities.stops.view.IStopsView;
import com.example.andresvelasquez.domiciliostest.network.request.stop.StopRequest;
import com.example.andresvelasquez.domiciliostest.network.response.StopsResponse;

/**
 * Created by AndresVelasquez on 5/03/18.
 */

public class StopsPresenter implements IStopsPresenter,StopRequest.OnStopsListener {
    private IStopsView stopsView;
    private IStopsInteractor stopsInteractor;

    public StopsPresenter(IStopsView stopsView) {
        this.stopsView = stopsView;
        this.stopsInteractor = new StopsInteractor();
    }

    /**
     * Se comunica con el interactor para proceder
     * con la peticion del listado de paradas del vehiculo
     */
    @Override
    public void onGetStops(String url) {
        stopsInteractor.getStops(this,url);
    }

    /**
     * Retorna el listado de paradas del vehiculo
     * @param stopsResponse
     */
    @Override
    public void didGetStops(StopsResponse stopsResponse) {
        stopsView.bindStops(stopsResponse);
    }

    /**
     * Retorna error en caso de que haya fallado la peticion del servicio
     * @param error
     */
    @Override
    public void didGetError(String error) {
        stopsView.showToast(error);
    }
}
