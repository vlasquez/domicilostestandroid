package com.example.andresvelasquez.domiciliostest.activities.stops.view;

import android.content.Intent;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.example.andresvelasquez.domiciliostest.R;
import com.example.andresvelasquez.domiciliostest.activities.stops.presenter.IStopsPresenter;
import com.example.andresvelasquez.domiciliostest.activities.stops.presenter.StopsPresenter;
import com.example.andresvelasquez.domiciliostest.models.Route;
import com.example.andresvelasquez.domiciliostest.network.response.StopsResponse;

public class StopActivity extends AppCompatActivity implements IStopsView {
    IStopsPresenter stopsPresenter;
    private StopsResponse stopsResponse;
    private Route route;
    private TextView txtEstimatedTime;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stop);
        stopsPresenter = new StopsPresenter(this);
        txtEstimatedTime = (TextView) findViewById(R.id.txtEstimatedTime);
        Log.d("stop","onCreate");

    }

    @Override
    protected void onResume() {
        super.onResume();
        Intent i = getIntent();
        route = (Route) i.getSerializableExtra("route");
        Log.d("stop","onResume");
        showProgress();
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d("stop","onStop");
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("stop","onDestroy");
    }

    @Override
    public void bindStops(StopsResponse stopsResponse) {
        this.stopsResponse = stopsResponse;
        //TODO: Enlazar con widget de google maps
        setEstimatedTime(this.stopsResponse.estimated_time_milliseconds);
    }

    @Override
    public void showProgress() {
        //TODO: Mostrar AlertDialog de progreso
        try{
            stopsPresenter.onGetStops(route.getStops_url());
        }
        catch (Exception ex)
        {
            showToast(ex.toString());
        }

    }
    private void setEstimatedTime(int time){
        CountDownTimer ct = new CountDownTimer(time,1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                String v = String.format("%02d", millisUntilFinished/60000);
                int va = (int)( (millisUntilFinished%60000)/1000);
                txtEstimatedTime.setText(v+":"+String.format("%02d",va));
            }

            @Override
            public void onFinish() {

            }
        };
        ct.start();

    }
    @Override
    public void showToast(String error) {
        Toast.makeText(this,error,Toast.LENGTH_SHORT).show();
    }
}
