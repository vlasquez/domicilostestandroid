package com.example.andresvelasquez.domiciliostest.activities.stops.presenter;

/**
 * Created by AndresVelasquez on 5/03/18.
 */

public interface IStopsPresenter {
   void onGetStops(String url);
}
